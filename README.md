# BowlingTabit - Avishai Peretz

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.

##Installing

Clone the repository to your machine
```
git clone git@bitbucket.org:Power1/bowlingtabit.git
```

Go to app folder and run npm install
```
cd bowliingTabit
npm install
```

To run this app you must clone [server project](https://bitbucket.org/Power1/bowlingtabitserver) 
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

