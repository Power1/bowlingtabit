import { Component } from '@angular/core';
import {ShareService} from "./services/share/share.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public shareService: ShareService) {

  }
}
