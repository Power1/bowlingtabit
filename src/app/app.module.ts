import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { enableProdMode } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2LoadingSpinnerModule } from 'ng2-loading-spinner';

import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {ApiService} from "./services/api/api.service";
import {ShareService} from "./services/share/share.service";
import { FrameComponent } from './components/frame/frame.component';
import { TopScoresComponent } from './components/top-scores/top-scores.component';


// Parent Routes
const routes : Routes = [
  {
    path: '',
    component: RegisterComponent,
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
  },
  {
    path: 'top-scores',
    component: TopScoresComponent,
  },
];

enableProdMode();
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    RegisterComponent,
    FrameComponent,
    TopScoresComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    RouterModule.forRoot(routes),
    Ng2LoadingSpinnerModule.forRoot({})
  ],
  providers: [
    ApiService,
    ShareService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
