import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class ApiService {
  baseUrl:string = 'http://localhost:4000/api/';
  httpOptions:any = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) { }

  get(url) {
    return this.http.get(this.baseUrl + url, this.httpOptions);
  }

  post(url, params) {
    return this.http.post(this.baseUrl + url, params, this.httpOptions);
  }
}
