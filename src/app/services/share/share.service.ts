import { Injectable } from '@angular/core';
import {FrameComponent} from "../../components/frame/frame.component";

@Injectable()
export class ShareService {
  userName:string = '';
  frames:Array<FrameComponent>;
  showLoading:boolean = false;

  constructor() { }
}
