import {
  ChangeDetectorRef,
  Component,
  OnInit,
  QueryList,
  ViewChildren,
  ViewContainerRef,
  ViewEncapsulation
} from '@angular/core';
import {ShareService} from "../../services/share/share.service";
import {Router} from "@angular/router";
import {FrameComponent} from "../frame/frame.component";
import {ApiService} from "../../services/api/api.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {
  @ViewChildren(FrameComponent) frameComponents: QueryList<FrameComponent>;
  frames:Array<FrameComponent> = new Array<FrameComponent>(10);
  pins:Array<number> = Array.from(Array(11).keys());
  currentFrame:FrameComponent;
  activeFrameIndex:number = 0;
  gameReady:boolean = false;
  gamedEnded:boolean = false;
  totalScore:number;
  highlightType:string = '';

  constructor(private shareService:ShareService, private router:Router, private cdRef : ChangeDetectorRef, private apiService:ApiService) { }

  initGame() {
    this.activeFrameIndex = 0;
    this.currentFrame = this.frameComponents.first;
    this.shareService.frames = this.frameComponents.toArray();

    this.gameReady = true;
  }

  knockDown(pin) {
    this.currentFrame.knockDown(pin);
  }

  goToNextFrame(data) {
    const nextFrameIndex = data.frameIndex + 1;
    this.currentFrame = this.frameComponents.toArray()[nextFrameIndex];
    this.activeFrameIndex = nextFrameIndex;
  }

  endGame(totalScore) {
    this.shareService.showLoading = true;

    this.gamedEnded = true;
    this.totalScore = totalScore;

    let data:any = {
      user: this.shareService.userName,
      totalScore: totalScore,
      frames: this.shareService.frames.map((frame:FrameComponent) => {
        return {
          isStrike: frame.isStrike,
          isSpare: frame.isSpare,
          isLastFrame: frame.isLastFrame,
          rolls: frame.rolls,
          total: frame.total,
        }
      })
    }

    this.apiService.post('saveScore', data).subscribe((res:any) => {
      this.shareService.showLoading = false;
      if (!res._id) {
        alert('Error: Failed to save data in DB.');
      }
    }, (error) => {
      this.shareService.showLoading = false;
      alert(JSON.stringify(error));
    });
  }

  startNewGame() {
    this.router.navigate(['']);
  }

  showHighlight(type) {
    this.highlightType = type;

    setTimeout(() => {
      this.highlightType = '';
    }, 1500);
  }

  ngOnInit() {
    if (!this.shareService.userName) {
      this.router.navigate(['']);
    }
  }

  ngAfterViewInit() {
    this.initGame();
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
}
