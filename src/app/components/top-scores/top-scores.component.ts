import {ChangeDetectorRef, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ShareService} from "../../services/share/share.service";
import {ApiService} from "../../services/api/api.service";

@Component({
  selector: 'app-top-scores',
  templateUrl: './top-scores.component.html',
  styleUrls: ['./top-scores.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TopScoresComponent implements OnInit {
  data:any = [];

  constructor(private shareService:ShareService, private apiService:ApiService, private cdRef : ChangeDetectorRef) { }

  loadData() {
    this.shareService.showLoading = true;
    this.apiService.get('scores').subscribe((res:any) => {
      this.data = res;
      this.shareService.showLoading = false;
    }, (error) => {
      this.shareService.showLoading = false;
      alert('Error to get scores data.');
    })
  }

  ngOnInit() {
    this.loadData();
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
}
