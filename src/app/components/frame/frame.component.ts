import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {ShareService} from "../../services/share/share.service";

@Component({
  selector: 'app-frame',
  templateUrl: './frame.component.html',
  styleUrls: ['./frame.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class FrameComponent implements OnInit {
  @Input() isViewMode:boolean = false;
  @Input() viewModeData:any;

  @Input() index:number;
  @Output() goToNextFrame = new EventEmitter<any>();
  @Output() endGame = new EventEmitter<number>();
  @Output() showHighlight = new EventEmitter<string>();

  rolls:Array<number>;
  total:number = 0;
  rollIndex:number = 0;
  pinsLeft:number = 10;
  showTotal:boolean = false;
  isStrike:boolean = false;
  isSpare:boolean = false;
  needUpdateStrikeByNextSecond:boolean = false;
  isLastFrame:boolean = false;
  lastFrameRollThird:boolean = false;

  constructor(private shareService:ShareService) { }

  updatePrevFrameBySpareBonus() {
    let prevFrame = this.shareService.frames[this.index - 2];
    if (prevFrame.isSpare || this.isStrike) {
      prevFrame.total += this.rolls[0];
      prevFrame.showTotal = this.isStrike === false;
      prevFrame.needUpdateStrikeByNextSecond = this.isStrike;
    }

    if (this.index > 2) {
      let prevSecondFrame = this.shareService.frames[this.index - 3];
      if (prevSecondFrame.needUpdateStrikeByNextSecond) {
        prevSecondFrame.total += this.rolls[0];
        prevFrame.total += this.rolls[0];
        prevSecondFrame.needUpdateStrikeByNextSecond = false;
        prevSecondFrame.showTotal = true;
      }
    }
  }

  updatePrevFrameByStrikeBonus() {
    let prevFrame = this.shareService.frames[this.index - 2];
    if (prevFrame.isStrike) {
      prevFrame.total += this.rolls[0] + this.rolls[1];
      prevFrame.showTotal = true;
    }
  }

  knockDown(pins) {

    this.pinsLeft -= pins;
    this.rolls[this.rollIndex] = pins;

    this.isStrike = !this.isLastFrame && this.rollIndex === 0 && pins === 10;
    this.isSpare = !this.isLastFrame && this.rollIndex === 1 && this.pinsLeft === 0;

    if (this.index > 1) {

      if (this.rollIndex === 0) {
        this.updatePrevFrameBySpareBonus();
      }

      if (this.rollIndex === 1) {
        this.updatePrevFrameByStrikeBonus();
      }
    }

    this.total += pins;

    if (this.isStrike || (this.isLastFrame && pins === 10)) {
      this.showHighlight.emit('strike');
    }

    if (this.isSpare || (this.isLastFrame && this.rollIndex === 1 && this.rolls[0] + this.rolls[1] === 10)) {
      this.showHighlight.emit('spare');
    }

    if (this.isLastFrame && (this.rollIndex === 0 && pins === 10) || (this.rollIndex === 1 && this.rolls[0] + this.rolls[1] === 10)) {
      this.lastFrameRollThird = true;
    }

    this.rollIndex++;

    if (this.isLastFrame && this.rolls[0] === 10) {
      this.pinsLeft = 10;
    }

    if (this.isLastFrame && this.rollIndex === 2 && this.lastFrameRollThird) {
      this.pinsLeft = 10;
    }

    if (this.isStrike || this.rollIndex === this.rolls.length || (this.isLastFrame && this.rollIndex === 2 && !this.lastFrameRollThird)) {
      if (this.index > 1) {
        let prevFrame = this.shareService.frames[this.index - 2];
        this.total += prevFrame.total;
      }

      this.showTotal = !this.isSpare && !this.isStrike;

      if (this.isLastFrame) {
        if ((this.rollIndex > 1 && !this.lastFrameRollThird) || this.rollIndex > 2) {
          this.endGame.emit(this.total);
        }
      } else {
        this.goToNextFrame.emit({
          frameIndex: this.index-1
        });
      }
    }
  }

  ngOnInit() {
    if (this.isViewMode) {
      this.rolls = this.viewModeData.rolls;
      this.total = this.viewModeData.total;
      this.isStrike = this.viewModeData.isStrike;
      this.isSpare = this.viewModeData.isSpare;
      this.isLastFrame = this.viewModeData.isLastFrame;
      this.showTotal = true;

    } else {
      this.isLastFrame = this.index === 10;

      if (this.isLastFrame) {
        this.rolls = new Array<number>(3);
      } else {
        this.rolls = new Array<number>(2);
      }
    }
  }

}
