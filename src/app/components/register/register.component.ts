import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {ShareService} from "../../services/share/share.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {
  formObj:any;

  constructor(private formBuilder:FormBuilder, private shareService:ShareService, private router:Router) {
    this.formObj = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, this.noWhitespaceValidator, Validators.minLength(2), Validators.maxLength(20)])]
    });

  }

  submit() {
    if (!this.formObj.valid) {
      return;
    }

    this.shareService.userName = this.formObj.value.name;

    this.router.navigate(['dashboard']);
  }

  noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  ngOnInit() {
    this.shareService.userName = '';
  }

}
